# spconv1.2.1 安装资源文件

本仓库提供了一个用于安装 spconv 1.2.1 的资源文件，经过亲测，该文件在 Python 3.6、3.7 和 3.8 环境下均有效。

## 资源文件描述

- **文件名称**: spconv1.2.1 安装文件
- **适用环境**: Python 3.6, 3.7, 3.8

## 使用说明

1. 下载本仓库中的资源文件。
2. 按照 spconv 的官方安装指南进行安装。
3. 确保你的 Python 环境为 3.6、3.7 或 3.8，以保证安装的兼容性。

## 注意事项

- 请确保在安装前已安装必要的依赖库。
- 如果在安装过程中遇到问题，请参考 spconv 的官方文档或社区支持。

希望这个资源文件能够帮助你顺利安装 spconv 1.2.1！